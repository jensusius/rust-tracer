# Rust Tracer

An exercise to learn rust by porting ["Ray Tracing In One Weekend"](https://raytracing.github.io/books/RayTracingInOneWeekend.html).

Generally followed the design choices from the tutorial and modified wherever it does not fit the rust paradigm.

# Future efforts
Convert the design and data structures into rust idiom and attempt to make the tracer multi threaded.

![example](example.png)