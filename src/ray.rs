use crate::vec3::{Vec3, Point3};

#[derive(Debug)]
pub struct Ray<'a> {
    pub origin: &'a Point3,
    pub direction: Vec3
}

impl<'a> Ray<'a> {
    pub fn at(& self, t: f64) -> Point3{
        self.origin + &(&self.direction * t)
    }
}