extern crate rand;

use rand::Rng;

pub mod camera;
pub mod hittable;
pub mod ray;
pub mod sphere;
pub mod vec3;
use camera::Camera;
use hittable::{hit_record, Hittable};
use ray::Ray;
use sphere::Sphere;
use vec3::{Color, Point3, Vec3};

const INFINITY: f64 = f64::INFINITY;
const PI: f64 = 3.1415926535897932385;

fn degrees_to_radians(degrees: f64) -> f64 {
    degrees * PI / 180.0
}

fn rand_float() -> f64 {
    // let mut rng = rand::thre
    let mut rng = rand::thread_rng();
    rng.gen_range(0.0, 1.0)
}

fn scan_hitlist(
    objects: &Vec<Box<dyn Hittable>>,
    r: &Ray,
    t_min: f64,
    t_max: f64,
) -> (bool, hit_record) {
    let mut temp_rec = hit_record {
        p: Vec3::new(0.0, 0.0, 0.0),
        normal: Vec3::new(0.0, 0.0, 0.0),
        t: t_min,
        front_face: false,
    };
    let mut hit_anything = false;
    let mut closest_so_far = t_max;
    // false
    for object in objects {
        if object.hit(r, t_min, closest_so_far, &mut temp_rec) {
            hit_anything = true;
            closest_so_far = temp_rec.t;
        }
    }
    return (hit_anything, temp_rec);
}

fn clamp(x: f64, min: f64, max: f64) -> f64 {
    if x < min {
        min
    } else if x > max {
        max
    } else {
        x
    }
}

fn write_color(pixel_color: &Color, samples_per_pixel: i32) {
    let scale = 1.0 / samples_per_pixel as f64;

    let r = (pixel_color.x * scale).sqrt();
    let g = (pixel_color.y * scale).sqrt();
    let b = (pixel_color.z * scale).sqrt();

    println!(
        "{:?} {:?} {:?}",
        (255.99 * clamp(r, 0.0, 0.999)) as i32,
        (255.99 * clamp(g, 0.0, 0.999)) as i32,
        (255.99 * clamp(b, 0.0, 0.999)) as i32
    )
}

fn random_in_unit_sphere() -> Vec3 {
    let mut p = Vec3::random_range(-1.0, 1.0);
    while p.length_squared() >= 1.0 {
        p = Vec3::random_range(-1.0, 1.0);
    }
    return p
}


fn hit_sphere(center: &Point3, radius: f64, r: &Ray) -> f64 {
    let oc: Vec3 = r.origin - center;
    let a = r.direction.length_squared();
    let half_b = oc.dot(&r.direction);
    let c = oc.length_squared() - radius * radius;
    let discriminant = half_b * half_b - a * c;
    if discriminant < 0.0 {
        -1.0
    } else {
        (-half_b - discriminant.sqrt()) / a
    }
}

fn ray_color(world: &Vec<Box<dyn Hittable>>, r: &Ray, depth: i32) -> Color {
    if depth <= 0 {
        return Color::new(0.0, 0.0, 0.0);
    }
    let (hit_anything, rec) = scan_hitlist(world, &r, 0.0, INFINITY);
    if hit_anything {
        let target = rec.p + rec.normal + random_in_unit_sphere();
        let rr = Ray{origin: &rec.p, direction: target - rec.p};
        return ray_color(world, &rr, depth - 1) * 0.5;
    }

    let unit_direction = r.direction.unit_vector();
    let t = 0.5 * (unit_direction.y + 1.0);
    Color::new(1.0, 1.0, 1.0) * (1.0 - t) + (Color::new(0.5, 0.7, 1.0) * t)
}

fn main() {
    let aspect_ratio = 16.0 / 9.0;
    let image_width = 1920;
    let image_height = (image_width as f64 / aspect_ratio) as i32;
    let samples_per_pixel = 30;
    let max_depth = 20;

    println!("P3");
    println!("{} {}", image_width, image_height);
    println!("255");

    let origin = Point3::new(0.0, 0.0, 0.0);
    let horizontal = Vec3::new(4.0, 0.0, 0.0);
    let vertical = Vec3::new(0.0, 2.25, 0.0);

    let lower_left_corner =
        &origin - &horizontal / 2.0 - &vertical / 2.0 - Vec3::new(0.0, 0.0, 1.0);
    let cam = Camera::new(origin, lower_left_corner, horizontal, vertical);

    let mut world: Vec<Box<dyn Hittable>> = Vec::new();
    world.push(Box::new(Sphere {
        center: Point3::new(0.0, 0.0, -1.0),
        radius: 0.5,
    }));
    world.push(Box::new(Sphere {
        center: Point3::new(0.0, -100.5, -1.0),
        radius: 100.0,
    }));

    for j in (0..image_height).rev() {
        eprintln!("lines remaining: {:?}", j);
        for i in 0..image_width {
            let mut pixel_color = Color::new(0.0, 0.0, 0.0);
            for _ in 0..samples_per_pixel {
                let u = (i as f64 + rand_float()) / (image_width - 1) as f64;
                let v = (j as f64 + rand_float()) / (image_height - 1) as f64;
                let r = cam.get_ray(u, v);
                pixel_color = pixel_color + ray_color(&world, &r, max_depth);
            }
            write_color(&pixel_color, samples_per_pixel);
        }
    }
}
